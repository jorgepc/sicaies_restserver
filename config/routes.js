/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  // HTML Views
  //'GET /': { view: 'index' },

  '/auth/start'  : 'login/Peticiones_inicio_sesionController.start',
  '/auth/login'  : 'login/UsuariosController.login',
  '/auth/logout' : 'login/UsuariosController.logout',
  
  '/main/menu_tree': 'principal/Menu_formatosController.menu_tree',
  
  //Rutas para la seccion de formatos de arribo de embarcaciones
  '/avisos/lista_formato_arribo'                        : 'avisos/ArriboembarcacionesController.lista_formato_arribo',
  '/avisos/lista_captura_formato_arribo'                : 'avisos/ArriboembarcacionesController.lista_captura_formato_arribo',
  '/avisos/actualiza_aviso_arribo_embarcacion'          : 'avisos/ArriboembarcacionesController.actualiza_aviso_arribo_embarcacion',
  '/avisos/envia_formato_revision_arribo_embarcacion'   : 'avisos/ArriboembarcacionesController.envia_formato_revision_arribo_embarcacion',
  '/avisos/guarda_revision_formato_arribo_embarcacion'  : 'avisos/ArriboembarcacionesController.guarda_revision_formato_arribo_embarcacion',
  '/avisos/rechaza_formato_revision_arribo_embarcacion' : 'avisos/ArriboembarcacionesController.rechaza_formato_revision_arribo_embarcacion',
  '/avisos/acepta_formato_revision_arribo_embarcacion'  : 'avisos/ArriboembarcacionesController.acepta_formato_revision_arribo_embarcacion',
  '/avisos/elimina_formato_revision_arribo_embarcacion' : 'avisos/ArriboembarcacionesController.elimina_formato_revision_arribo_embarcacion',
  '/avisos/genera_documento_pdf_arribo_emb'             : 'avisos/ArriboembarcacionesController.genera_documento_pdf_arribo_emb',
  '/avisos/descargapdfarriboemb'                        : 'avisos/ArriboembarcacionesController.descargapdfarriboemb',
  
  '/avisos/lista_formato_cosecha'                       : 'avisos/AvisoCosechaController.lista_formato_cosecha',
  '/avisos/lista_captura_formato_cosecha'               : 'avisos/AvisoCosechaController.lista_captura_formato_cosecha',
  '/avisos/actualiza_aviso_cosecha'                     : 'avisos/AvisoCosechaController.actualiza_aviso_cosecha',
  '/avisos/envia_formato_revision_aviso_cosecha'        : 'avisos/AvisoCosechaController.envia_formato_revision_aviso_cosecha',
  '/avisos/guarda_revision_formato_aviso_cosecha'       : 'avisos/AvisoCosechaController.guarda_revision_formato_aviso_cosecha',
  '/avisos/rechaza_formato_revision_aviso_cosecha'      : 'avisos/AvisoCosechaController.rechaza_formato_revision_aviso_cosecha',
  '/avisos/acepta_formato_revision_aviso_cosecha'       : 'avisos/AvisoCosechaController.acepta_formato_revision_aviso_cosecha',
  '/avisos/elimina_formato_revision_aviso_cosecha'      : 'avisos/AvisoCosechaController.elimina_formato_revision_aviso_cosecha',
  '/avisos/genera_documento_pdf_aviso_cosecha'          : 'avisos/AvisoCosechaController.genera_documento_pdf_aviso_cosecha',
  '/avisos/descargapdfavisocosecha'                     : 'avisos/AvisoCosechaController.descargapdfavisocosecha',
  
  
  '/catalogos/localidades'                              : 'catalogos/CatalogosController.localidades',
  '/catalogos/puertos'                                  : 'catalogos/CatalogosController.puertos',
  '/catalogos/lugar_captura'                            : 'catalogos/CatalogosController.lugar_captura',
  '/catalogos/lista_especies_pesca'                     : 'catalogos/CatalogosController.lista_especies_pesca',  
  '/catalogos/delegaciones'                             : 'catalogos/CatalogosController.delegaciones',
  '/catalogos/oficinas'                                 : 'catalogos/CatalogosController.oficinas',
  '/catalogos/municipios'                               : 'catalogos/CatalogosController.municipios',
  '/catalogos/estados'                                  : 'catalogos/CatalogosController.estados',
  '/catalogos/lista_especies_cosecha'                   : 'catalogos/CatalogosController.lista_especies_cosecha',
  '/catalogos/permisionarios'                           : 'catalogos/CatalogosController.permisionarios',


  '/admin/listuser'                             : 'admin/AdminController.listuser',
  '/admin/listformatosPermisionario'            : 'admin/AdminController.listformatosPermisionario',
  '/admin/listformatosDisponibles'              : 'admin/AdminController.listformatosDisponibles',
  '/admin/addedituser'                          : 'admin/AdminController.addedituser',
  '/admin/deleteuser'                           : 'admin/AdminController.deleteuser',
  '/admin/bota_logueo'                          : 'admin/AdminController.bota_logueo',
  '/admin/modifica_permisionario'               : 'admin/AdminController.modifica_permisionario',
  '/admin/elimina_permisionario'                : 'admin/AdminController.elimina_permisionario',
  //'/admin/agrega_formato_permisionario'       : 'admin/AdminController.agrega_formato_permisionario',
  //'/admin/elimina_formato_permisionario'      : 'admin/AdminController.elimina_formato_permisionario',
  '/admin/agrega_elimina_formato_permisionario' : 'admin/AdminController.agrega_elimina_formato_permisionario',
  '/admin/modifica_catalogos'                   : 'admin/AdminController.modifica_catalogos',
  '/admin/elimina_catalogos'                    : 'admin/AdminController.elimina_catalogos'



  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  *  If a request to a URL doesn't match any of the custom routes above, it  *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
