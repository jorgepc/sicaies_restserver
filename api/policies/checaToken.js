module.exports = function checaToken (req, res, next) {

  if(req.originalUrl == '/auth/logout')
    next();
  else if(req.param('tki') === undefined)
  {
        var token = req.param('t');

        Usuarios.findOne({
          token_sesion: token
        }).exec(function (err, usuario) {

            if (err) return res.negotiate(err);

            if (!usuario) {
                return res.ok({
                          error: -2,
                          msg: 'Usted no tiene permiso para consultar este servicio'
                      });
            }
            else if(usuario.logueado == 1)
            {
                  next(); 
            }
            else
            {
                return res.ok({
                          error: -1,
                          msg: 'usuario no logueado'
                      });
            }

        });

  }
  else
    next();

};