/**
 * UsuariosController
 *
 * @description :: Server-side logic for managing usuarios
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
  /**
   * `UsuariosController.login()`
   */
  login: function (req, res) {

        if(req.param('tki') === 'undefined')
        {
            return res.ok({
                success : false,
                msg     : 'Token inválido, favor de recargar la aplicación'               
            });          
        }

        var usuario     = req.param('u');
        var password    = req.param('p');
        var key_inicio = req.param('tki');

        //Validamos el token de inicio de sesion
        Peticiones_inicio_sesion.findOne({
             key_inicio: key_inicio
        }).exec(function (err, peticion) {

              if (!peticion) {
                      return res.ok({
                          success : false,
                          msg     : 'Token de inicio de sesion no encontrado, favor de recargar la aplicación'
                      });
              }
              else
              {
                      Usuarios.findOne({
                        usuario: usuario
                      }).populate('permisionario').exec(function (err, usuario) {

                              if (err) return res.negotiate(err);

                              if (!usuario) {
                                      return res.ok({
                                          success : false,
                                          msg     : 'Usuario no encontrado'
                                      });
                              }
                              else
                              {
                                      //Checar si el usuario ya esta logueado y si es asi no permitir su logueo
                                      var crypto        = require('crypto');

                                      var pass          = crypto.createHash('sha512').update( (usuario.pass + key_inicio) ).digest("hex");
                            
                                      if(pass === password)
                                      {
                                          if(usuario.logueado == 1 && usuario.perfil < 3)
                                          {
                                                return res.ok({
                                                    success : false,
                                                    msg     : 'El usuario ya inició sesión'
                                                });
                                          }
                                          else
                                          {
                                                var id_usuario    = usuario.id_usuario;
                                                var nombre        = usuario.nombre;
                                                var perfil        = usuario.perfil;
                                                var permisionario = usuario.permisionario;

                                                if(perfil == 1)
                                                {
                                                  var id_permisionario     = permisionario.id_permisionario;
                                                  var nombre_permisionario = permisionario.nombre_permisionario;
                                                  var clave_rnp            = permisionario.clave_rnp;
                                                }
                                                else if(perfil == 2)
                                                {
                                                  var id_permisionario     = 0;
                                                  var nombre_permisionario = 'REVISOR';
                                                  var clave_rnp            = '';
                                                }
                                                else if(perfil == 3)
                                                {
                                                  var id_permisionario     = 0;
                                                  var nombre_permisionario = 'ADMIN';
                                                  var clave_rnp            = '';
                                                }

                                                var uuid = require('node-uuid');
                                                var crypto = require('crypto');

                                                var randomid = uuid.v4();
                                                var key_final = crypto.createHash('sha512').update(randomid).digest("hex");

                                                Usuarios.update({
                                                    id_usuario : id_usuario
                                                },
                                                {
                                                    logueado : 1,
                                                    fecha_logueo : new Date(),
                                                    token_sesion : key_final 
                                                })
                                                .exec(function (err, usuario){

                                                    Peticiones_inicio_sesion.update({
                                                        key_inicio : key_inicio
                                                    },
                                                    {
                                                        logueado : 1,
                                                        id_usuario : id_usuario 
                                                    })
                                                    .exec(function (err, resultado){

                                                        return res.ok({
                                                              success : true,
                                                              tm : key_final,
                                                              user: {
                                                                  id_usuario : id_usuario,
                                                                  nombre: nombre,
                                                                  perfil: perfil,
                                                                  id_permisionario: id_permisionario,
                                                                  nombre_permisionario: nombre_permisionario,
                                                                  clave_rnp:clave_rnp
                                                              }
                                                        });                                    
                                                    });
                                                });

                                          }

                                      }
                                      else
                                      {
                                          return res.ok({
                                              success : false,
                                              msg     : 'Password incorrecto'
                                          });
                                      }
                              }

                      });

              }

        });
  },


  /**
   * `UsuariosController.logout()`
   */
  logout: function (req, res) {
        var token_sesion = req.param('t');
        var key_inicio   = req.param('tc');

        Usuarios.update({
            token_sesion : token_sesion
        },
        {
            logueado : 0,
            fecha_salida : new Date()
        })
        .exec(function (err, usuario){

                Peticiones_inicio_sesion.update({
                    key_inicio : key_inicio
                },
                {
                    logueado : 0
                })
                .exec(function (err, usuario){
                    return res.ok({success : true});                                    
                });
        });
  }



};

