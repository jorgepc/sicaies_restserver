/**
 * Peticiones_inicio_sesionController
 *
 * @description :: Server-side logic for managing peticiones_inicio_sesions
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
  /**
   * `Peticiones_inicio_sesionController.start()`
   */
  start: function (req, res) {
	    // Attempt to signup a user using the provided parameters
		var requestIp = require('request-ip');
		var clientIp = requestIp.getClientIp(req); 

	    if(req.param('tki') === ''||req.param('tki') === undefined)
	    {
				var uuid = require('node-uuid');
				var randomid = uuid.v4();
				
				var crypto = require('crypto');
				var key_inicio = crypto.createHash('sha512').update(randomid).digest("hex");

			    Peticiones_inicio_sesion.create({
					ip         : clientIp,
					key_inicio : key_inicio
			    })
			    .exec(function (err, peticion){
			        return res.ok({
						success    : true,
						id_estatus : 0,
						i          : peticion.id_peticion,
						tc         : peticion.key_inicio
			        });		    	
			    });
	    }
	    else
	    {	    	
				Peticiones_inicio_sesion.findOne({
					key_inicio: req.param('tki')
				}).populate('usuario').exec(function (err, peticion) {
					  //if (err) return res.negotiate(err);
					  if (err){
					        return res.ok({
								success    : false,
								id_estatus : -3,
								msg        : 'Acceso a la aplicacion no autorizado'
					        });		    	
					  }
					  
					  if (!peticion) {
								var uuid = require('node-uuid');
								var randomid = uuid.v4();
								
								var crypto = require('crypto');
								var key_inicio = crypto.createHash('sha512').update(randomid).digest("hex");

							    Peticiones_inicio_sesion.create({
									ip         : clientIp,
									key_inicio : key_inicio
							    })
							    .exec(function (err, peticion){
							        return res.ok({
										success    : true,
										id_estatus : 0,
										i          : peticion.id_peticion,
										tc         : peticion.key_inicio
							        });		    	
							    });
					  }
					  else
					  {
					  		if(peticion.logueado == false)
					  		{
						        return res.ok({
									success    : true,
									id_estatus : 0,
									i          : peticion.id_peticion,
									tc         : peticion.key_inicio
						        });
					  		}
					  		else
					  		{
					  			var usuario = peticion.usuario;

			                    var fechaResta = new Date() - new Date(usuario.fecha_logueo);
			                    var dif_minutos = ((fechaResta/1000)/60);

			                    if(dif_minutos >=30)
			                    {
		                                return res.ok({
											success    : false,
											id_estatus : -4,
											msg        : 'sesion timeout'
		                                });
			                    }
			                    else
			                    {
								        return res.ok({
											success : true,
											id_estatus : 0,
											tm      : usuario.token_sesion
								        });
			                    }


					  		}
					  }

				});
	    }
   }
};


