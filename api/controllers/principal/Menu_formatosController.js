/**
 * Principal/menu_formatosController
 *
 * @description :: Server-side logic for managing principal/menu_formatos
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

	menu_tree: function (req, res) {
   
        var parent_id = req.param('node');
        if(parent_id == 'root')
            parent_id = 0;

            var resultado = new Array();
        Menu_formatos.find({
          parent_id : parent_id,
          tipo : {'!':'M'}
        }).exec(function (err, menu) {
            
            for(var i =0;i<menu.length;i++)
            {
                var elemento = menu[i];

                if(elemento.leaf == 'true')
                {
                    var record = {
                        id: elemento.nodo_id,
                        opcion_id: elemento.opcion_id,
                        text: elemento.text,
                        leaf: elemento.leaf,
                        cls: 'nodos-menu-modulos'
                    };
                }
                else
                {
                    var record = {
                        id: elemento.nodo_id,
                        opcion_id: elemento.opcion_id,
                        text: elemento.text,
                        cls: 'nodos-menu-modulos'
                    };
                }
                resultado.push(record);
            };

                    Permisionarios.findOne({
                      id_permisionario: req.param('id_permisionario')
                    })
                    .populate('formatos',{parent_id: parent_id})
                    .exec(function (err, permisionario) {
                        
                        //console.log(permisionario.formatos.length);
                        //return res.ok(permisionario);
                        if(req.param('id_permisionario') > 0)
                        {
                                for(var i =0;i<permisionario.formatos.length;i++)
                                {
                                    var elemento = permisionario.formatos[i];

                                    if(elemento.leaf == 'true')
                                    {
                                        var record = {
                                            id: elemento.nodo_id,
                                            opcion_id: elemento.opcion_id,
                                            text: elemento.text,
                                            leaf: elemento.leaf,
                                            cls: 'nodos-menu-modulos'
                                        };
                                    }
                                    else
                                    {
                                        var record = {
                                            id: elemento.nodo_id,
                                            opcion_id: elemento.opcion_id,
                                            text: elemento.text,
                                            cls: 'nodos-menu-modulos'
                                        };
                                    }
                                    resultado.push(record);
                                };
                        }
                        
                        return res.ok(JSON.stringify(resultado));
                    });


            //return res.jsonx(JSON.stringify(resultado));
        });
   }

	
};
