/**
 * CatalogosController
 *
 * @description :: Server-side logic for managing catalogos
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
  /**
   * `CatalogosController.localidades()`
   */
  localidades: function (req, res) {
      Catalogo_localidades.find()
      .sort('nombre_localidad ASC')
      .exec(function (err, resultado) {
            return res.ok({
                total   : resultado.length,
                results : resultado
            });                      
      });
  },


  /**
   * `CatalogosController.puertos()`
   */
  puertos: function (req, res) {

      if(req.param('start') != undefined)
      {
        var start = req.param('start');
        var limit = req.param('limit');
      }

      Catalogo_puertos.count()
      .exec(function countCB(error, total) {

            var total_registros = total;

            Catalogo_puertos.find()
            .paginate({page: start, limit: limit})
            .sort('nombre_puerto ASC')
            .exec(function (err, resultado) {

                    return res.ok({
                        success : true,
                        total   : total_registros,
                        results : resultado
                    });                      
            });
      });
  },


  /**
   * `CatalogosController.lugar_captura()`
   */
  lugar_captura: function (req, res) {

      if(req.param('start') != undefined)
      {
        var start = req.param('start');
        var limit = req.param('limit');
      }

      Catalogo_lugares_captura.count()
      .exec(function countCB(error, total) {

            var total_registros = total;

            Catalogo_lugares_captura.find()
            .paginate({page: start, limit: limit})
            .sort('descripcion ASC')
            .exec(function (err, resultado) {

                    return res.ok({
                        success : true,
                        total   : total_registros,
                        results : resultado
                    });                      
            });
      });
  },


  /**
   * `CatalogosController.lista_especies()`
   */
  lista_especies_pesca: function (req, res) {

      if(req.param('start') != undefined)
      {
        var start = req.param('start');
        var limit = req.param('limit');
      }

      Catalogo_especies_pesca.count()
      .exec(function countCB(error, total) {

            var total_registros = total;

            Catalogo_especies_pesca.find()
            .paginate({page: start, limit: limit})
            .sort('nombre_especie ASC')
            .exec(function (err, resultado) {

                    return res.ok({
                        success : true,
                        total   : total_registros,
                        results : resultado
                    });                      
            });
      });
  },


  /**
   * `CatalogosController.delegaciones()`
   */
  delegaciones: function (req, res) {

      if(req.param('start') != undefined)
      {
        var start = req.param('start');
        var limit = req.param('limit');
      }

      Catalogo_delegaciones.count()
      .exec(function countCB(error, total) {

            var total_registros = total;

            Catalogo_delegaciones.find()
            .paginate({page: start, limit: limit})
            .sort('nombre_delegacion ASC')
            .exec(function (err, resultado) {

                    return res.ok({
                        success : true,
                        total   : total_registros,
                        results : resultado
                    });                      
            });
      });
  },


  /**
   * `CatalogosController.oficinas()`
   */
  oficinas: function (req, res) {

      if(req.param('start') != undefined)
      {
        var start = req.param('start');
        var limit = req.param('limit');
      }

      Catalogo_oficinas.count({id_delegacion: req.param('id_delegacion')})
      .exec(function countCB(error, total) {

            var total_registros = total;

            Catalogo_oficinas.find({id_delegacion: req.param('id_delegacion')})
            .paginate({page: start, limit: limit})
            .sort('nombre_oficina ASC')
            .exec(function (err, resultado) {

                    return res.ok({
                        success : true,
                        total   : total_registros,
                        results : resultado
                    });                      
            });
      });
  },

  /**
   * `CatalogosController.municipios()`
   */
  municipios: function (req, res) {
      Catalogo_municipios.find()
      .sort('nombre_municipio ASC')
      .exec(function (err, resultado) {
            return res.ok({
                total   : resultado.length,
                results : resultado
            });                      
      });
  },

  /**
   * `CatalogosController.estados()`
   */
  estados: function (req, res) {
      Catalogo_estados.find()
      .sort('nombre_estado ASC')
      .exec(function (err, resultado) {
            return res.ok({
                total   : resultado.length,
                results : resultado
            });                      
      });
  },


  /**
   * `CatalogosController.lista_especies_cosecha()`
   */
  lista_especies_cosecha: function (req, res) {

      if(req.param('start') != undefined)
      {
        var start = req.param('start');
        var limit = req.param('limit');
      }

      Catalogo_especies_cosecha.count()
      .exec(function countCB(error, total) {

            var total_registros = total;

            Catalogo_especies_cosecha.find()
            .paginate({page: start, limit: limit})
            .sort('nombre_especie ASC')
            .exec(function (err, resultado) {

                    return res.ok({
                        success : true,
                        total   : total_registros,
                        results : resultado
                    });                      

                    /*var registro = new Array();
                    
                    registro.push({
                        id_especie        : 0,
                        nombre_especie    : 'Seleccione una especie',
                        clave_especie     : '',              
                        nombre_cientifico : ''               
                    });

                    for(var i =0;i<resultado.length;i++)
                    {
                        var elemento = resultado[i];

                            var record = {
                                id_especie        : elemento.id_especie,
                                nombre_especie    : elemento.nombre_especie,
                                clave_especie     : elemento.clave_especie,
                                nombre_cientifico : elemento.nombre_cientifico
                            };

                            registro.push(record);
                    };  
                    return res.ok({
                        success : true,
                        total   : total_registros,
                        results : registro
                    });  */                    
            });
      });
  },

  /**
   * `CatalogosController.permisionarios()`
   */
  permisionarios: function (req, res) {

      if(req.param('start') != undefined)
      {
        var start = req.param('start');
        var limit = req.param('limit');
      }
      else
      {
        var start = 0;
        var limit = 999999;        
      }

      Permisionarios.count()
      .exec(function countCB(error, total) {

            var total_registros = total;

            Permisionarios.find()
            .paginate({page: start, limit: limit})
            .sort('nombre_permisionario ASC')
            .exec(function (err, resultado) {
                  return res.ok({
                      success : true,
                      total   : total_registros,
                      results : resultado
                  });                      
            });
      });

  }


};

