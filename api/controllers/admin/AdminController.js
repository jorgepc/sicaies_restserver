/**
 * Principal/menu_formatosController
 *
 * @description :: Server-side logic for managing principal/menu_formatos
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  /**
   * `AdminController.listuser()`
   */
  listuser: function (req, res) {

        if(req.param('start') != undefined)
        {
          var start = req.param('start');
          var limit = req.param('limit');
        }

        Usuarios.find({
          perfil: 1
        })
        .paginate({page: start, limit: limit})
        .populate('permisionario')
        .exec(function (err, usuario) {

                if (err) return res.negotiate(err);

                if (!usuario) {
                      return res.ok({
                          total   : 0,
                          results : ''
                      });
                }
                else
                {
                      var dt = require('datetimejs');
                      dt.AM = 'AM';
                      dt.PM = 'PM';
                      dt.MONTHS = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];

                      var resultado = new Array();
                      for(var i =0;i<usuario.length;i++)
                      {
                          var elemento = usuario[i];

/*                              if(elemento.permisionario === undefined)
                              {
                                  var id_permisionario = 0;
                                  var tipo_permisionario = 'R';
                                  if(elemento.perfil == 2)
                                      var nombre_permisionario = 'REVISORES';
                                  else
                                      var nombre_permisionario = '';
                              }
                              else
                              {
                                  var id_permisionario     = elemento.permisionario.id_permisionario;
                                  var tipo_permisionario   = elemento.permisionario.tipo_permisionario;
                                  var nombre_permisionario = elemento.permisionario.nombre_permisionario;
                              }
*/
                              var record = {
                                  id_usuario           : elemento.id_usuario,
                                  id_permisionario     : elemento.permisionario.id_permisionario,
                                  nombre_permisionario : elemento.permisionario.nombre_permisionario,
                                  clave_rnp            : elemento.permisionario.clave_rnp,
                                  tipo_permisionario   : elemento.permisionario.tipo_permisionario,
                                  nombre_persona       : elemento.nombre,
                                  nombre_usuario       : elemento.usuario,
                                  perfil               : elemento.perfil,
                                  logueado             : elemento.logueado == false ? 0 : 1,
                                  fecha_logueo         : dt.strftime(new Date(elemento.fecha_logueo), '%m-%d-%Y %i:%M %p'),
                                  fecha_salida         : dt.strftime(new Date(elemento.fecha_salida), '%m-%d-%Y %i:%M %p')
                              };

                              resultado.push(record);
                      };  

                      //Para ordenar el objeto por un dato en particular
                      var resultado_ordenado = resultado.slice(0);
                      resultado_ordenado.sort(function(a,b) {
                          var x = a.nombre_permisionario.toLowerCase();
                          var y = b.nombre_permisionario.toLowerCase();
                          return x < y ? -1 : x > y ? 1 : 0;
                      });

                      return res.ok({
                          total   : resultado_ordenado.length,
                          results : resultado_ordenado
                      });                      

                }

        });
  },

  /**
   * `AdminController.listformatosPermisionario()`
   */
  listformatosPermisionario: function (req, res) {

      Permisionarios.findOne({
        id_permisionario: req.param('id_permisionario')
      })
      .populate('formatos')
      .exec(function (err, permisionario) {

                if (err) return res.negotiate(err);

                if (!permisionario) {
                      return res.ok({
                          total   : 0,
                          results : ''
                      });
                }
                else
                {
                      var lista_formatos = permisionario.formatos;
                      
                      if (lista_formatos === undefined) {
                          return res.ok({
                              total   : 0,
                              results : ''
                          });                        
                      }
                      else
                      {
                          return res.ok({
                              total   : lista_formatos.length,
                              results : lista_formatos
                          });                        
                      }

                }

      });
  },

  /**
   * `AdminController.listformatosDisponibles()`
   */
  listformatosDisponibles: function (req, res) {
      
      if(req.param('id_permisionario') === undefined)
      {
          return res.ok({
              total   : 0,
              results : ''
          });
      }
      var id_permisionario = parseInt(req.param('id_permisionario'));

      Menu_formatos.find({
        tipo: 'M'
      })
      .populate('permisionarios', { id_permisionario: id_permisionario } )
      .exec(function (err, lista_formatos) {

                if (err) return res.negotiate(err);

                if (!lista_formatos) {
                      return res.ok({
                          total   : 0,
                          results : ''
                      });
                }
                else
                {
                      var resultado = new Array();
                      for(var i =0;i<lista_formatos.length;i++)
                      {
                          var elemento = lista_formatos[i];


                              if(elemento.permisionarios.length > 0)
                                  var checked = true;
                              else
                                  var checked = false;

                              var record = {
                                  nodo_id : elemento.nodo_id,
                                  text    : elemento.text,
                                  checked : checked
                              };

                              resultado.push(record);
                      };  

                      //Para ordenar el objeto por un dato en particular
                      var resultado_ordenado = resultado.slice(0);
                      resultado_ordenado.sort(function(a,b) {
                          var x = a.text.toLowerCase();
                          var y = b.text.toLowerCase();
                          return x < y ? -1 : x > y ? 1 : 0;
                      });

                      return res.ok({
                          total   : resultado_ordenado.length,
                          results : resultado_ordenado
                      });                      
                }

      });
  },

  /**
   * `AdminController.adduser()`
   */
  addedituser: function (req, res) {
      if(req.param('te') == 0)
      {
            Usuarios.create({
                  permisionario : req.param('id_permisionario'),
                  nombre        : req.param('nombre'),
                  usuario       : req.param('usuario'),
                  pass          : req.param('pass'),
                  perfil        : 1
            })
            .exec(function (err, usuario){
                  
                  if (err) return res.negotiate(err);

                  return res.ok({success : true});
            });

      }
      else if(req.param('te') == 1)
      {
            Usuarios.update({
                  id_usuario: req.param('id_usuario')
            },
            {
                  permisionario : req.param('id_permisionario'),
                  nombre        : req.param('nombre'),
                  usuario       : req.param('usuario')
            })
            .exec(function (err, usuario){

                  if (err) return res.negotiate(err);

                  if(req.param('pass') != '')
                  {
                        Usuarios.update({
                              id_usuario: req.param('id_usuario')
                        },
                        {
                              pass : req.param('pass')
                        })
                        .exec(function (err, usuario_login){
                              if (err) return res.negotiate(err);
                              return res.ok({success : true});
                        });

                  }
                  else
                        return res.ok({success : true});

            });

      }
  },


  /**
   * `AdminController.deleteuser()`
   */
  deleteuser: function (req, res) {

        Usuarios.destroy({
              id_usuario: req.param('id_usuario')
        })
        .exec(function (err, eliminados){
              if (err) return res.negotiate(err); 
              
              return res.ok({success : true});
        });
  },

  /**
   * `AdminController.deleteuser()`
   */
  bota_logueo: function (req, res) {

        Usuarios.update({
              id_usuario: req.param('id_usuario')
        },
        {
              logueado : 0
        })
        .exec(function (err, eliminados){
              if (err) return res.negotiate(err); 
              
              return res.ok({success : true});
        });
  },


  modifica_permisionario: function (req, res) {
   
        var campo = req.param('campo');
        var valor = req.param('valor');
        var actualizacion;

        if(campo == 'nombre_permisionario')
            actualizacion = {nombre_permisionario : valor};
        else if(campo == 'clave_rnp')
            actualizacion = {clave_rnp : valor};
        else if(campo == 'tipo_permisionario')
            actualizacion = {tipo_permisionario : valor};

        if(req.param('id_permisionario') == 0)
        {
                Permisionarios.create(actualizacion).exec(function (err, permisionario){

                      if (err) return res.negotiate(err);

                      return res.ok({
                        success : true,
                        newID: permisionario.id_permisionario
                    });
                });
        }
        else
        {
                Permisionarios.update({
                    id_permisionario: req.param('id_permisionario')
                },
                    actualizacion
                )
                .exec(function (err, permisionario){

                      if (err) return res.negotiate(err);

                      return res.ok({success : true});
                });
        }
  },


  elimina_permisionario: function (req, res) {

        Permisionarios.destroy({
              id_permisionario: req.param('id_permisionario')
        })
        .exec(function (err, eliminados){
              if (err) return res.negotiate(err); 
        });

        return res.ok({success : true});
  },

  /*agrega_formato_permisionario: function (req, res) {
        Permisionarios.findOne({
            id_permisionario: req.param('id_permisionario')
        })
        .populate('formatos').exec(function(err, permisionario) { 

            permisionario.formatos.add(req.param('nodo_id')); 
            
            permisionario.save(function(err) {
            });

            return res.ok({success : true});
        });
  },

  elimina_formato_permisionario: function (req, res) {

        console.log("id_permisionario: "+req.param('id_permisionario'));
        console.log("nodo_id: "+req.param('nodo_id'));
        console.log("-----------------------------------------");

        Permisionarios.findOne({
            id_permisionario: req.param('id_permisionario')
        })
        .populate('formatos').exec(function(err, permisionario) { 
            
            permisionario.formatos.remove(req.param('nodo_id')); 
            
            permisionario.save(function(err) {
            });

            return res.ok({success : true});
        });
  },*/

  agrega_elimina_formato_permisionario: function (req, res) {

        //console.log("id_permisionario: "+req.param('id_permisionario'));
        //console.log("ids: "+req.param('ids'));
        
        Permisionarios.findOne({
            id_permisionario: req.param('id_permisionario')
        })
        .populate('formatos').exec(function(err, permisionario) { 
            
                //console.log(permisionario);

                if(permisionario.formatos.length > 0)
                {
                        //primero eliminamos todos los formatos relacionados a este permisionario
                        for(var i =0;i<permisionario.formatos.length;i++)
                        {
                                //console.log("id_formato eliminar: "+permisionario.formatos[i].nodo_id);
                                permisionario.formatos.remove(permisionario.formatos[i].nodo_id); 
                        };
                                permisionario.save(function(err,r) {
                                    //console.log("*****************************************************");
                                    //console.log("siguiente");

                                            Permisionarios.findOne({
                                                id_permisionario: req.param('id_permisionario')
                                            })
                                            .populate('formatos').exec(function(err, permisionario) { 
                                                
                                                    //luego agregamos todos los formatos
                                                    
                                                    var array_ids = req.param('ids').split(',');
                                                    for(var i =0;i<array_ids.length;i++)
                                                    {
                                                            nodo_id =array_ids[i];
                                                            //console.log("nodo_id agregado: "+nodo_id);
                                                            permisionario.formatos.add(nodo_id); 
                                                            
                                                    }

                                                            permisionario.save(function(err,r) {
                                                                //console.log("guardado");
                                                                //console.log("||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");
                                                               
                                                            });

                                                            
                                            });

                                });
                        
                }
                else
                {
                        Permisionarios.findOne({
                            id_permisionario: req.param('id_permisionario')
                        })
                        .populate('formatos').exec(function(err, permisionario) { 
                            
                                //luego agregamos todos los formatos
                                
                                var array_ids = req.param('ids').split(',');
                                for(var i =0;i<array_ids.length;i++)
                                {
                                        nodo_id =array_ids[i];
                                        //console.log("nodo_id agregado: "+nodo_id);
                                        permisionario.formatos.add(nodo_id); 
                                        
                                }

                                        permisionario.save(function(err,r) {
                                            //console.log("guardado");
                                            //console.log("||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");
                                            
                                        });

                                        
                        });
                }
                
                return res.ok({success : true});
        });
  },


  modifica_catalogos: function (req, res) {
   
        if(req.param('cat') == undefined)
            return res.ok({success : false});
        
        var campo = req.param('campo');
        var valor = req.param('valor');

        var cadena = '{'+campo+':"'+valor+'"}';
        var actualizacion = eval("("+cadena+")");

        if (req.param('cat') == 'lista_especies_cosecha')
        {
                if(req.param('id') == 0)
                {
                        Catalogo_especies_cosecha.create(actualizacion).exec(function (err, resultado){
                              if (err) return res.negotiate(err);
                              return res.ok({success : true, newID: resultado.id_especie});
                        });
                }
                else
                {
                        Catalogo_especies_cosecha.update({id_especie: req.param('id')}, actualizacion)
                        .exec(function (err, resultado){
                              if (err) return res.negotiate(err);
                              return res.ok({success : true});
                        });
                }
        }
        else if (req.param('cat') == 'lista_especies_pesca')
        {
                if(req.param('id') == 0)
                {
                        Catalogo_especies_pesca.create(actualizacion).exec(function (err, resultado){
                              if (err) return res.negotiate(err);
                              return res.ok({success : true, newID: resultado.id_especie});
                        });
                }
                else
                {
                        Catalogo_especies_pesca.update({id_especie: req.param('id')}, actualizacion)
                        .exec(function (err, resultado){
                              if (err) return res.negotiate(err);
                              return res.ok({success : true});
                        });
                }
        }
        else if (req.param('cat') == 'lugar_captura')
        {
                if(req.param('id') == 0)
                {
                        Catalogo_lugares_captura.create(actualizacion).exec(function (err, resultado){
                              if (err) return res.negotiate(err);
                              return res.ok({success : true, newID: resultado.id_lugar_captura});
                        });
                }
                else
                {
                        Catalogo_lugares_captura.update({id_lugar_captura: req.param('id')}, actualizacion)
                        .exec(function (err, resultado){
                              if (err) return res.negotiate(err);
                              return res.ok({success : true});
                        });
                }
        }
        else if (req.param('cat') == 'puertos')
        {
                if(req.param('id') == 0)
                {
                        Catalogo_puertos.create(actualizacion).exec(function (err, resultado){
                              if (err) return res.negotiate(err);
                              return res.ok({success : true, newID: resultado.id_puerto});
                        });
                }
                else
                {
                        Catalogo_puertos.update({id_puerto: req.param('id')}, actualizacion)
                        .exec(function (err, resultado){
                              if (err) return res.negotiate(err);
                              return res.ok({success : true});
                        });
                }
        }
        else if (req.param('cat') == 'delegaciones')
        {
                if(req.param('id') == 0)
                {
                        Catalogo_delegaciones.create(actualizacion).exec(function (err, resultado){
                              if (err) return res.negotiate(err);
                              return res.ok({success : true, newID: resultado.id_delegacion});
                        });
                }
                else
                {
                        Catalogo_delegaciones.update({id_delegacion: req.param('id')}, actualizacion)
                        .exec(function (err, resultado){
                              if (err) return res.negotiate(err);
                              return res.ok({success : true});
                        });
                }
        }
        else if (req.param('cat') == 'oficinas')
        {
                if(req.param('id') == 0)
                {
                        cadena = '{'+campo+':"'+valor+'", id_delegacion: '+req.param('id_delegacion_oficina')+'}';
                        actualizacion = eval("("+cadena+")");

                        Catalogo_oficinas.create(actualizacion).exec(function (err, resultado){
                              if (err) return res.negotiate(err);
                              return res.ok({success : true, newID: resultado.id_delegacion});
                        });
                }
                else
                {
                        Catalogo_oficinas.update({id_oficina: req.param('id')}, actualizacion)
                        .exec(function (err, resultado){
                              if (err) return res.negotiate(err);
                              return res.ok({success : true});
                        });
                }
        }
  },


  elimina_catalogos: function (req, res) {

        if (req.param('cat') == 'lista_especies_cosecha')
        {
                Catalogo_especies_cosecha.destroy({id_especie: req.param('id')})
                .exec(function (err, eliminados){
                      if (err) return res.negotiate(err); 
                });                
        }
        else if (req.param('cat') == 'lista_especies_pesca')
        {
                Catalogo_especies_pesca.destroy({id_especie: req.param('id')})
                .exec(function (err, eliminados){
                      if (err) return res.negotiate(err); 
                });                
        }
        else if (req.param('cat') == 'lugar_captura')
        {
                Catalogo_lugares_captura.destroy({id_lugar_captura: req.param('id')})
                .exec(function (err, eliminados){
                      if (err) return res.negotiate(err); 
                });                
        }
        else if (req.param('cat') == 'puertos')
        {
                Catalogo_puertos.destroy({id_puerto: req.param('id')})
                .exec(function (err, eliminados){
                      if (err) return res.negotiate(err); 
                });                
        }
        else if (req.param('cat') == 'delegaciones')
        {
                Catalogo_delegaciones.destroy({id_delegacion: req.param('id')})
                .exec(function (err, eliminados){
                      if (err) return res.negotiate(err); 
                });                
        }
        else if (req.param('cat') == 'oficinas')
        {
                Catalogo_oficinas.destroy({id_oficina: req.param('id')})
                .exec(function (err, eliminados){
                      if (err) return res.negotiate(err); 
                });                
        }

        return res.ok({success : true});
  },

	
};
