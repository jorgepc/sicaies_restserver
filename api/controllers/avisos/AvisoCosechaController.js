/**
 * ArriboembarcacionesController
 *
 * @description :: Server-side logic for managing arriboembarcaciones
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
  /**
   * `ArriboembarcacionesController.lista_formato_arribo()`
   */
  lista_formato_cosecha: function (req, res) {
        
        var token = req.param('t');

        Usuarios.findOne({
          token_sesion: token
        }).exec(function (err, usuario) {

            if (err) return res.negotiate(err);

            if (!usuario) {
                    return res.ok({
                        total   : 0,
                        results : ''
                    });
            }
            else
            {
                  var perfil = usuario.perfil;
                  var id_usuario = usuario.id_usuario;
                  var id_permisionario = usuario.permisionario;

                  if(perfil == 1)
                    var condicion = {permisionario: id_permisionario};
                  else if(perfil == 2)
                    var condicion = {or : [{ estatus: 0 },{ estatus: 2 },{ estatus: 3 }]};
                  else
                    var condicion = {};

                  if(req.param('start') != undefined)
                  {
                    var start = req.param('start');
                    var limit = req.param('limit');
                  }

                  Lista_avisos_cosecha.find(condicion)
                  .paginate({page: start, limit: limit})
                  .populate('localidades')
                  .populate('delegaciones')
                  .populate('oficinas')
                  .populate('permisionario')
                  .populate('municipios')
                  .populate('estados')
                  .populate('detalle')
                  .sort('fecha DESC')
                  .sort('folio DESC')
                  .exec(function (err, listas) {

                        if (err) return res.negotiate(err); 
                        
                        if(!listas)
                        {
                            return res.ok({
                                total   : 0,
                                results : ''
                            });                      
                        }
                        else
                        {
                              var dt = require('datetimejs');
                              dt.AM = 'AM';
                              dt.PM = 'PM';
                              dt.MONTHS = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];

                              var resultado = new Array();
                              for(var i =0;i<listas.length;i++)
                              {
                                  var elemento = listas[i];

                                      var record = {
                                          id_formato                    : elemento.id_formato,
                                          folio                         : elemento.folio,
                                          id_localidad                  : elemento.localidades.id_localidad,
                                          fecha                         : dt.strftime(new Date(elemento.fecha), '%m-%d-%Y'),
                                          nombre_localidad              : elemento.localidades.nombre_localidad,
                                          id_delegacion                 : elemento.delegaciones.id_delegacion,
                                          nombre_delegacion             : elemento.delegaciones.nombre_delegacion,
                                          id_oficina                    : elemento.oficinas.id_oficina,
                                          nombre_oficina                : elemento.oficinas.nombre_oficina,
                                          id_permisionario              : elemento.permisionario.id_permisionario,
                                          nombre_permisionario          : elemento.permisionario.nombre_permisionario,
                                          clave_rnp                     : elemento.permisionario.clave_rnp,
                                          num_concesion                 : elemento.num_concesion,
                                          fecha_concesion               : dt.strftime(new Date(elemento.fecha_concesion), '%m-%d-%Y'),
                                          ubicacion_instalaciones       : elemento.ubicacion_instalaciones,
                                          id_municipio                  : elemento.municipios.id_municipio,
                                          nombre_municipio              : elemento.municipios.nombre_municipio,
                                          domicilio                     : elemento.domicilio,
                                          id_estado                     : elemento.estados.id_estado,
                                          nombre_estado                 : elemento.estados.nombre_estado,
                                          telefono                      : elemento.telefono,
                                          observaciones_revisor         : elemento.observaciones_revisor,
                                          estatus                       : elemento.estatus,
                                          num_elementos_lista           : elemento.detalle.length,
                                          fecha_envio_revision          : dt.strftime(new Date(elemento.fecha_envio_revision), '%m-%d-%Y %i:%M %p'),
                                          fecha_aceptacion              : dt.strftime(new Date(elemento.fecha_aceptacion), '%m-%d-%Y %i:%M %p')
                                      };

                                      resultado.push(record);
                              };  
                              return res.ok({
                                  total   : resultado.length,
                                  results : resultado
                              });                      
                        }
                  });
            }
          
        });

  },


  /**
   * `ArriboembarcacionesController.lista_captura()`
   */
  lista_captura_formato_cosecha: function (req, res) {

        var id_formato = req.param('id_formato');

                  Detalle_lista_avisos_cosecha.find({
                    id_formato: id_formato
                  })
                  .populate('especies', { sort: 'nombre_especie ASC' })
                  //.sort('Catalogo_especies.nombre_especie DESC')
                  .exec(function (err, detalle) {

                        if (err) return res.negotiate(err); 
                        
                        if(!detalle)
                        {
                            return res.ok({
                                total   : 0,
                                results : ''
                            });                      
                        }
                        else
                        {
                              var resultado = new Array();

                              //Otra forma de recorrer el objeto y su contenido
                              while (detalle.length)
                              {
                                  var elemento = detalle.pop();

                                      var record = {
                                          id                : elemento.id,
                                          id_especie        : elemento.especies.id_especie,
                                          nombre_especie    : elemento.especies.nombre_especie,
                                          clave_especie     : elemento.especies.clave_especie,
                                          nombre_cientifico : elemento.especies.nombre_cientifico,
                                          presentacion      : elemento.presentacion,
                                          volumen           : elemento.volumen,
                                          precio_kg         : elemento.precio_kg
                                      };

                                      resultado.push(record);
                              };
                              
                            //Para ordenar el objeto por un dato en particular
                            var resultado_ordenado = resultado.slice(0);
                            resultado_ordenado.sort(function(a,b) {
                                var x = a.nombre_especie.toLowerCase();
                                var y = b.nombre_especie.toLowerCase();
                                return x < y ? -1 : x > y ? 1 : 0;
                            });

                            return res.ok({
                                total   : resultado.length,
                                results : resultado_ordenado
                            });                      
                        }
                  });
  },

  /**
   * `ArriboembarcacionesController.actualiza_aviso_aviso_cosecha()`
   */
  actualiza_aviso_cosecha: function (req, res) {

      var dt = require('datetimejs');
      dt.AM = 'AM';
      dt.PM = 'PM';

      if(req.param('te') == 0)
      {

            Lista_avisos_cosecha.create({
                  //id_formato                    : { type: 'integer', unique: true, primaryKey: true},
                  folio                   : '',
                  localidades             : req.param('id_localidad'),
                  fecha                   : dt.datetime.toUTC(new Date(req.param('fecha'))),
                  delegaciones            : req.param('id_delegacion'),
                  oficinas                : req.param('id_oficina'),
                  permisionario           : req.param('id_permisionario'),
                  num_concesion           : req.param('num_concesion'),
                  fecha_concesion         : dt.datetime.toUTC(new Date(req.param('fecha_concesion'))),
                  ubicacion_instalaciones : req.param('ubicacion_instalaciones'),
                  municipios              : req.param('id_municipio'),
                  domicilio               : req.param('domicilio'),
                  estados                 : req.param('id_estado'),
                  telefono                : req.param('telefono'),
                  observaciones_revisor   : '',
                  estatus                 : 1
            })
            .exec(function (err, lista_creada){
                  
                  if (err) return res.negotiate(err);

                  var lista_especies = JSON.parse(req.param('lista_especies'));
                  var id_formato = lista_creada.id_formato;
                  
                  while (lista_especies.length)
                  {
                        var elemento = lista_especies.pop();

                        Detalle_lista_avisos_cosecha.create({
                              formato          : id_formato,
                              especies         : elemento.id_especie,
                              presentacion     : elemento.presentacion,
                              volumen          : elemento.volumen,
                              precio_kg        : elemento.precio_kg
                        })
                        .exec(function (err, lista){
                              if (err) return res.negotiate(err);   
                        });
                  };

                  return res.ok({success : true});
            });

      }
      else if(req.param('te') == 1)
      {
            Lista_avisos_cosecha.update({
                  id_formato: req.param('id_formato')
            },
            {
                  localidades             : req.param('id_localidad'),
                  fecha                   : dt.datetime.toUTC(new Date(req.param('fecha'))),
                  delegaciones            : req.param('id_delegacion'),
                  oficinas                : req.param('id_oficina'),
                  permisionario           : req.param('id_permisionario'),
                  num_concesion           : req.param('num_concesion'),
                  fecha_concesion         : dt.datetime.toUTC(new Date(req.param('fecha_concesion'))),
                  ubicacion_instalaciones : req.param('ubicacion_instalaciones'),
                  municipios              : req.param('id_municipio'),
                  domicilio               : req.param('domicilio'),
                  estados                 : req.param('id_estado'),
                  telefono                : req.param('telefono'),
                  estatus                 : 1
            })
            .exec(function (err, lista_creada){

                  Detalle_lista_avisos_cosecha.destroy({
                        formato: req.param('id_formato')
                  })
                  .exec(function (err, registro_borrados){

                        var lista_especies = JSON.parse(req.param('lista_especies'));
                        
                        while (lista_especies.length)
                        {
                              var elemento = lista_especies.pop();

                              Detalle_lista_avisos_cosecha.create({
                                    formato      : req.param('id_formato'),
                                    especies     : elemento.id_especie,
                                    presentacion : elemento.presentacion,
                                    volumen      : elemento.volumen,
                                    precio_kg    : elemento.precio_kg
                              })
                              .exec(function (err, lista){
                                    if (err) return res.negotiate(err);
                              });
                        };

                        return res.ok({success : true});
                  });
            });

      }
      else
      {

      }
  },


  /**
   * `ArriboembarcacionesController.guarda_revision_formato_revision_aviso_cosecha()`
   */
  guarda_revision_formato_aviso_cosecha: function (req, res) {

            Lista_avisos_cosecha.update({
                  id_formato: req.param('id_formato')
            },
            {
                  observaciones_revisor : req.param('texto')
            })
            .exec(function (err, lista_creada){
                  if (err) return res.negotiate(err); 
                  
                  return res.ok({success : true});
            });

  },


  /**
   * `ArriboembarcacionesController.envia_formato_revision_aviso_cosecha()`
   */
  envia_formato_revision_aviso_cosecha: function (req, res) {

            Lista_avisos_cosecha.update({
                  id_formato: req.param('id_formato')
            },
            {
                  estatus : 2,
                  fecha_envio_revision: new Date()
            })
            .exec(function (err, lista_creada){
                  if (err) return res.negotiate(err); 
                  
                  return res.ok({success : true});
            });

  },

  /**
   * `ArriboembarcacionesController.rechaza_formato_revision_aviso_cosecha()`
   */
  rechaza_formato_revision_aviso_cosecha: function (req, res) {

            Lista_avisos_cosecha.update({
                  id_formato: req.param('id_formato')
            },
            {
                  estatus : 3
            })
            .exec(function (err, lista_creada){
                  if (err) return res.negotiate(err); 
                  
                  return res.ok({success : true});
            });

  },

  /**
   * `ArriboembarcacionesController.acepta_formato_revision_aviso_cosecha()`
   */
  acepta_formato_revision_aviso_cosecha: function (req, res) {

            Lista_avisos_cosecha.update({
                  id_formato: req.param('id_formato')
            },
            {
                  estatus : 0,
                  fecha_aceptacion: new Date()
            })
            .exec(function (err, lista_creada){
                  if (err) return res.negotiate(err); 
                  
                  return res.ok({success : true});
            });

  },

  /**
   * `ArriboembarcacionesController.elimina_formato_revision_arribo_embarcacion()`
   */
  elimina_formato_revision_aviso_cosecha: function (req, res) {

            Lista_avisos_cosecha.destroy({
                  id_formato: req.param('id_formato')
            })
            .exec(function (err, eliminados){
                  if (err) return res.negotiate(err); 
                  
                  return res.ok({success : true});
            });

  },


  /**
   * `ArriboembarcacionesController.genera_documento_pdf_arribo_emb()`
   */
  genera_documento_pdf_aviso_cosecha: function (req, res) {

                  Lista_avisos_cosecha.findOne({id_formato: req.param('id_formato')})
                  .populate('localidades')
                  .populate('delegaciones')
                  .populate('oficinas')
                  .populate('permisionario')
                  .populate('municipios')
                  .populate('estados')
                  .populate('detalle')
                  .sort('fecha DESC')
                  .sort('folio DESC')
                  .exec(function (err, lista) {

                        if (err) return res.negotiate(err); 
                        
                        if(!lista)
                        {
                            return res.ok({
                                total   : 0,
                                results : ''
                            });                      
                        }
                        else
                        {
                                  var dt = require('datetimejs');
                                  dt.AM = 'AM';
                                  dt.PM = 'PM';
                                  dt.MONTHS = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
                                  //dt.MNTH = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Agos','Sep','Oct','Nov','Dic'];

                                  var record = {
                                          id_formato                    : lista.id_formato,
                                          folio                         : lista.folio,
                                          id_localidad                  : lista.localidades.id_localidad,
                                          fecha                         : dt.strftime(new Date(lista.fecha), '%d/%B/%Y'),
                                          nombre_localidad              : lista.localidades.nombre_localidad,
                                          id_delegacion                 : lista.delegaciones.id_delegacion,
                                          nombre_delegacion             : lista.delegaciones.nombre_delegacion,
                                          id_oficina                    : lista.oficinas.id_oficina,
                                          nombre_oficina                : lista.oficinas.nombre_oficina,
                                          id_permisionario              : lista.permisionario.id_permisionario,
                                          nombre_permisionario          : lista.permisionario.nombre_permisionario,
                                          clave_rnp                     : lista.permisionario.clave_rnp,
                                          num_concesion                 : lista.num_concesion,
                                          fecha_concesion               : dt.strftime(new Date(lista.fecha_concesion), '%d/%B/%Y'),
                                          ubicacion_instalaciones       : lista.ubicacion_instalaciones,
                                          id_municipio                  : lista.municipios.id_municipio,
                                          nombre_municipio              : lista.municipios.nombre_municipio,
                                          domicilio                     : lista.domicilio,
                                          id_estado                     : lista.estados.id_estado,
                                          nombre_estado                 : lista.estados.nombre_estado,
                                          telefono                      : lista.telefono,
                                          observaciones_revisor         : lista.observaciones_revisor,
                                          estatus                       : lista.estatus,
                                          num_elementos_lista           : lista.detalle.length,
                                          fecha_envio_revision          : dt.strftime(new Date(lista.fecha_envio_revision), '%d/%B/%Y %i:%M %p'),
                                          fecha_aceptacion              : dt.strftime(new Date(lista.fecha_aceptacion), '%d/%B/%Y %i:%M %p')
                                  };


                                  Detalle_lista_avisos_cosecha.find({
                                    id_formato: req.param('id_formato')
                                  })
                                  .populate('especies')
                                  .exec(function (err, detalle) {
                                          var resultado = new Array();

                                          //Otra forma de recorrer el objeto y su contenido
                                          while (detalle.length)
                                          {
                                              var elemento = detalle.pop();

                                                  var record_detalle = {
                                                      id                : elemento.id,
                                                      id_especie        : elemento.especies.id_especie,
                                                      nombre_especie    : elemento.especies.nombre_especie,
                                                      clave_especie     : elemento.especies.clave_especie,
                                                      nombre_cientifico : elemento.especies.nombre_cientifico,
                                                      presentacion      : elemento.presentacion,
                                                      volumen           : elemento.volumen,
                                                      precio_kg         : elemento.precio_kg
                                                  };

                                                  resultado.push(record_detalle);
                                          };
                                          
                                        //Para ordenar el objeto por un dato en particular
                                        var resultado_ordenado = resultado.slice(0);
                                        resultado_ordenado.sort(function(a,b) {
                                            var x = a.nombre_especie.toLowerCase();
                                            var y = b.nombre_especie.toLowerCase();
                                            return x < y ? -1 : x > y ? 1 : 0;
                                        });

                                        record.detalle = resultado_ordenado;
                                        //sails.log.info(record);
                                        var fileFolder = 'archivos/pdf_generados/aviso_cosecha/';
                                        var fileName = 'f'+req.param('id_formato') + '.pdf';
                                        var filePath =  fileFolder + fileName;

                                        var fs = require('fs-extra');

                                        // Check if file exists
                                        fs.exists(filePath, function(exists) {
                                          if (exists)
                                          {  
                                                fs.unlink(filePath, function(){
                                                });
                                          }
                                              
                                                servicioPDF.generaPDF(record, 'aviso_cosecha', function(error){
                                                  if(error){
                                                    sails.log.error(error);
                                                    return res.ok({
                                                       estatus: -1
                                                    });
                                                  }
                                                  return res.ok({
                                                     estatus: 0,
                                                     fileid: fileName
                                                  });
                                                  
                                                });
                                        });

                                  });

                        }
                  });

  },

  /**
   * `ArriboembarcacionesController.descargapdf()`
   */
  descargapdfavisocosecha: function (req, res) {
      var fileFolder = 'archivos/pdf_generados/aviso_cosecha/';
      var filePath =  fileFolder + req.param('fileid');
      res.download(filePath,req.param('fileid'));
  }

};

