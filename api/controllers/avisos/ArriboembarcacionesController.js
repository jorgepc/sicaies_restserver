/**
 * ArriboembarcacionesController
 *
 * @description :: Server-side logic for managing arriboembarcaciones
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
  /**
   * `ArriboembarcacionesController.lista_formato_arribo()`
   */
  lista_formato_arribo: function (req, res) {
        
        var token = req.param('t');

        Usuarios.findOne({
          token_sesion: token
        }).exec(function (err, usuario) {

            if (err) return res.negotiate(err);

            if (!usuario) {
                    return res.ok({
                        total   : 0,
                        results : ''
                    });
            }
            else
            {
                  var perfil = usuario.perfil;
                  var id_usuario = usuario.id_usuario;
                  var id_permisionario = usuario.permisionario;

                  if(perfil == 1)
                    var condicion = {permisionario: id_permisionario};
                  else if(perfil == 2)
                    var condicion = {or : [{ estatus: 0 },{ estatus: 2 },{ estatus: 3 }]};
                  else
                    var condicion = {};

                  if(req.param('start') != undefined)
                  {
                    var start = req.param('start');
                    var limit = req.param('limit');
                  }

                  Lista_avisos_arribo_emb.find(condicion)
                  .paginate({page: start, limit: limit})
                  .populate('localidades')
                  .populate('puertos')
                  .populate('permisionario')
                  .populate('lugares_captura')
                  .populate('detalle')
                  .sort('fecha DESC')
                  .sort('folio DESC')
                  .exec(function (err, listas) {

                        if (err) return res.negotiate(err); 
                        
                        if(!listas)
                        {
                            return res.ok({
                                total   : 0,
                                results : ''
                            });                      
                        }
                        else
                        {
                              var dt = require('datetimejs');
                              dt.AM = 'AM';
                              dt.PM = 'PM';
                              dt.MONTHS = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
                              //dt.MNTH = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Agos','Sep','Oct','Nov','Dic'];
                              //var now = new Date();

                              var resultado = new Array();
                              for(var i =0;i<listas.length;i++)
                              {
                                  var elemento = listas[i];

                                      var record = {
                                          id_formato                    : elemento.id_formato,
                                          folio                         : elemento.folio,
                                          fecha                         : dt.strftime(new Date(elemento.fecha), '%m-%d-%Y'), //dateFormat(elemento.fecha, "UTC:dd/mmm/yyyy"),
                                          id_localidad                  : elemento.localidades.id_localidad,
                                          nombre_localidad              : elemento.localidades.nombre_localidad,
                                          fecha_llegada                 : dt.strftime(new Date(elemento.fecha_llegada), '%m-%d-%Y'), //dateFormat(elemento.fecha_llegada, "UTC:dd/mmm/yyyy"),
                                          hora_llegada                  : dt.reformat(elemento.hora_llegada, '%H:%M:%S', '%i:%M %p'), //dateFormat('0001-01-01T'+elemento.hora_llegada, "UTC:h:MM TT"), 
                                          hora_arribo                   : dt.reformat(elemento.hora_arribo, '%H:%M:%S', '%i:%M %p'), //dateFormat('0001-01-01T'+elemento.hora_arribo, "UTC:h:MM TT"), 
                                          periodo_inicio                : dt.strftime(new Date(elemento.periodo_inicio), '%m-%d-%Y'), //dateFormat(elemento.periodo_inicio, "UTC:dd/mmm/yyyy"),
                                          periodo_fin                   : dt.strftime(new Date(elemento.periodo_fin), '%m-%d-%Y'), //dateFormat(elemento.periodo_fin, "UTC:dd/mmm/yyyy"),
                                          id_puerto                     : elemento.puertos.id_puerto,
                                          nombre_puerto                 : elemento.puertos.nombre_puerto,
                                          id_permisionario              : elemento.permisionario.id_permisionario,
                                          nombre_permisionario          : elemento.permisionario.nombre_permisionario,
                                          clave_rnp                     : elemento.permisionario.clave_rnp,
                                          numero_embarcaciones          : elemento.numero_embarcaciones,
                                          id_lugar_captura              : elemento.lugares_captura.id_lugar_captura,
                                          lugar_captura                 : elemento.lugares_captura.descripcion,
                                          zona_captura_litoral          : elemento.zona_captura_litoral,
                                          zona_captura_bahia            : elemento.zona_captura_bahia,
                                          zona_captura_aguas_estuarinas : elemento.zona_captura_aguas_estuarinas,
                                          zona_captura_aguas_cont       : elemento.zona_captura_aguas_cont,
                                          num_dias_efectivos            : elemento.num_dias_efectivos,
                                          observaciones_revisor         : elemento.observaciones_revisor,
                                          estatus                       : elemento.estatus,
                                          num_elementos_lista           : elemento.detalle.length,
                                          fecha_envio_revision          : dt.strftime(new Date(elemento.fecha_envio_revision), '%m-%d-%Y %i:%M %p'),
                                          fecha_aceptacion              : dt.strftime(new Date(elemento.fecha_aceptacion), '%m-%d-%Y %i:%M %p')
                                      };

                                      resultado.push(record);
                              };  
                              return res.ok({
                                  total   : resultado.length,
                                  results : resultado
                              });                      
                        }
                  });
            }
          
        });

  },


  /**
   * `ArriboembarcacionesController.lista_captura()`
   */
  lista_captura_formato_arribo: function (req, res) {

        var id_formato = req.param('id_formato');

                  Detalle_lista_avisos_arribo_emb.find({
                    id_formato: id_formato
                  })
                  .populate('especies', { sort: 'nombre_especie ASC' })
                  //.sort('Catalogo_especies.nombre_especie DESC')
                  .exec(function (err, detalle) {

                        if (err) return res.negotiate(err); 
                        
                        if(!detalle)
                        {
                            return res.ok({
                                total   : 0,
                                results : ''
                            });                      
                        }
                        else
                        {
                              var dt = require('datetimejs');
                              dt.MONTHS = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
                              //dt.MNTH = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Agos','Sep','Oct','Nov','Dic'];

                              var resultado = new Array();

                              //Otra forma de recorrer el objeto y su contenido
                              while (detalle.length)
                              {
                                  var elemento = detalle.pop();

                                      var record = {
                                          id               : elemento.id,
                                          id_especie       : elemento.especies.id_especie,
                                          nombre_especie   : elemento.especies.nombre_especie,
                                          clave_especie    : elemento.especies.clave_especie,
                                          presentacion     : elemento.presentacion,
                                          preservacion     : elemento.preservacion,
                                          num_concesion    : elemento.num_concesion,                                          
                                          fecha_expedicion : dt.strftime(new Date(elemento.fecha_expedicion), '%m-%d-%Y'), // dateFormat(elemento.fecha_expedicion, "UTC:dd/mmm/yyyy"),
                                          vigencia_al      : dt.strftime(new Date(elemento.vigencia_al), '%m-%d-%Y'), //dateFormat(elemento.vigencia_al, "UTC:dd/mmm/yyyy"),  "2015-01-12T05:00:00.000Z",
                                          peso             : elemento.peso,
                                          precio_kg        : elemento.precio_kg
                                      };

                                      resultado.push(record);
                              };
                              
                            //Para ordenar el objeto por un dato en particular
                            var resultado_ordenado = resultado.slice(0);
                            resultado_ordenado.sort(function(a,b) {
                                var x = a.nombre_especie.toLowerCase();
                                var y = b.nombre_especie.toLowerCase();
                                return x < y ? -1 : x > y ? 1 : 0;
                            });

                            return res.ok({
                                total   : resultado.length,
                                results : resultado_ordenado
                            });                      
                        }
                  });
  },


  /**
   * `ArriboembarcacionesController.actualiza_aviso_arribo_embarcacion()`
   */
  actualiza_aviso_arribo_embarcacion: function (req, res) {

      var dt = require('datetimejs');
      dt.AM = 'AM';
      dt.PM = 'PM';

      if(req.param('te') == 0)
      {
            Lista_avisos_arribo_emb.create({
                  //id_formato                    : { type: 'integer', unique: true, primaryKey: true},
                  folio                         : '',
                  fecha                         : dt.datetime.toUTC(new Date(req.param('fecha'))),
                  localidades                   : req.param('id_localidad'),
                  fecha_llegada                 : dt.datetime.toUTC(new Date(req.param('fecha_llegada'))),
                  hora_llegada                  : dt.reformat(req.param('hora_llegada'), '%i:%M %p', '%H:%M:%S'),
                  hora_arribo                   : dt.reformat(req.param('hora_arribo'), '%i:%M %p', '%H:%M:%S'),
                  periodo_inicio                : dt.datetime.toUTC(new Date(req.param('periodo_inicio'))),
                  periodo_fin                   : dt.datetime.toUTC(new Date(req.param('periodo_fin'))),
                  puertos                       : req.param('id_puerto'),
                  permisionario                 : req.param('id_permisionario'),
                  numero_embarcaciones          : req.param('numero_embarcaciones'),
                  lugares_captura               : req.param('id_lugar_captura'),
                  zona_captura_litoral          : req.param('zona_captura_litoral') == 'true' ? 1 : 0,
                  zona_captura_bahia            : req.param('zona_captura_bahia') == 'true' ? 1 : 0,
                  zona_captura_aguas_estuarinas : req.param('zona_captura_aguas_estuarinas') == 'true' ? 1 : 0,
                  zona_captura_aguas_cont       : req.param('zona_captura_aguas_cont') == 'true' ? 1 : 0,
                  num_dias_efectivos            : req.param('num_dias_efectivos'),
                  observaciones_revisor         : '',
                  estatus                       : 1
            })
            .exec(function (err, lista_creada){
                  
                  if (err) return res.negotiate(err);

                  var lista_especies = JSON.parse(req.param('lista_especies'));
                  var id_formato = lista_creada.id_formato;
                  
                  while (lista_especies.length)
                  {
                        var elemento = lista_especies.pop();

                        Detalle_lista_avisos_arribo_emb.create({
                              formato          : id_formato,
                              especies         : elemento.id_especie,
                              presentacion     : elemento.presentacion,
                              preservacion     : elemento.preservacion,
                              num_concesion    : elemento.num_concesion,
                              fecha_expedicion : dt.datetime.toUTC(new Date(elemento.fecha_expedicion)),
                              vigencia_al      : dt.datetime.toUTC(new Date(elemento.vigencia_al)),
                              peso             : elemento.peso,
                              precio_kg        : elemento.precio_kg
                        })
                        .exec(function (err, lista){
                              if (err) return res.negotiate(err);   
                        });
                  };

                  return res.ok({success : true});
            });

      }
      else if(req.param('te') == 1)
      {
            Lista_avisos_arribo_emb.update({
                  id_formato: req.param('id_formato')
            },
            {
                  fecha                         : dt.datetime.toUTC(new Date(req.param('fecha'))),
                  localidades                   : req.param('id_localidad'),
                  fecha_llegada                 : dt.datetime.toUTC(new Date(req.param('fecha_llegada'))),
                  hora_llegada                  : dt.reformat(req.param('hora_llegada'), '%i:%M %p', '%H:%M:%S'),
                  hora_arribo                   : dt.reformat(req.param('hora_arribo'), '%i:%M %p', '%H:%M:%S'),
                  periodo_inicio                : dt.datetime.toUTC(new Date(req.param('periodo_inicio'))),
                  periodo_fin                   : dt.datetime.toUTC(new Date(req.param('periodo_fin'))),
                  puertos                       : req.param('id_puerto'),
                  permisionario                 : req.param('id_permisionario'),
                  numero_embarcaciones          : req.param('numero_embarcaciones'),
                  lugares_captura               : req.param('id_lugar_captura'),
                  zona_captura_litoral          : req.param('zona_captura_litoral') == 'true' ? 1 : 0,
                  zona_captura_bahia            : req.param('zona_captura_bahia') == 'true' ? 1 : 0,
                  zona_captura_aguas_estuarinas : req.param('zona_captura_aguas_estuarinas') == 'true' ? 1 : 0,
                  zona_captura_aguas_cont       : req.param('zona_captura_aguas_cont') == 'true' ? 1 : 0,
                  num_dias_efectivos            : req.param('num_dias_efectivos'),
                  estatus                       : 1
            })
            .exec(function (err, lista_creada){

                  Detalle_lista_avisos_arribo_emb.destroy({
                        formato: req.param('id_formato')
                  })
                  .exec(function (err, registro_borrados){

                        var lista_especies = JSON.parse(req.param('lista_especies'));
                        
                        while (lista_especies.length)
                        {
                              var elemento = lista_especies.pop();

                              Detalle_lista_avisos_arribo_emb.create({
                                    formato          : req.param('id_formato'),
                                    especies         : elemento.id_especie,
                                    presentacion     : elemento.presentacion,
                                    preservacion     : elemento.preservacion,
                                    num_concesion    : elemento.num_concesion,
                                    fecha_expedicion : dt.datetime.toUTC(new Date(elemento.fecha_expedicion)),
                                    vigencia_al      : dt.datetime.toUTC(new Date(elemento.vigencia_al)),
                                    peso             : elemento.peso,
                                    precio_kg        : elemento.precio_kg
                              })
                              .exec(function (err, lista){
                                    if (err) return res.negotiate(err);
                              });
                        };

                        return res.ok({success : true});
                  });
            });

      }
      else
      {

      }
  },

  /**
   * `ArriboembarcacionesController.guarda_revision_formato_arribo_embarcacion()`
   */
  guarda_revision_formato_arribo_embarcacion: function (req, res) {

            Lista_avisos_arribo_emb.update({
                  id_formato: req.param('id_formato')
            },
            {
                  observaciones_revisor : req.param('texto')
            })
            .exec(function (err, lista_creada){
                  if (err) return res.negotiate(err); 
                  
                  return res.ok({success : true});
            });

  },

  /**
   * `ArriboembarcacionesController.envia_formato_revision_arribo_embarcacion()`
   */
  envia_formato_revision_arribo_embarcacion: function (req, res) {

            Lista_avisos_arribo_emb.update({
                  id_formato: req.param('id_formato')
            },
            {
                  estatus : 2,
                  fecha_envio_revision: new Date()
            })
            .exec(function (err, lista_creada){
                  if (err) return res.negotiate(err); 
                  
                  return res.ok({success : true});
            });

  },

  /**
   * `ArriboembarcacionesController.rechaza_formato_revision_arribo_embarcacion()`
   */
  rechaza_formato_revision_arribo_embarcacion: function (req, res) {

            Lista_avisos_arribo_emb.update({
                  id_formato: req.param('id_formato')
            },
            {
                  estatus : 3
            })
            .exec(function (err, lista_creada){
                  if (err) return res.negotiate(err); 
                  
                  return res.ok({success : true});
            });

  },

  /**
   * `ArriboembarcacionesController.acepta_formato_revision_arribo_embarcacion()`
   */
  acepta_formato_revision_arribo_embarcacion: function (req, res) {

            Lista_avisos_arribo_emb.update({
                  id_formato: req.param('id_formato')
            },
            {
                  estatus : 0,
                  fecha_aceptacion: new Date()
            })
            .exec(function (err, lista_creada){
                  if (err) return res.negotiate(err); 
                  
                  return res.ok({success : true});
            });

  },

  /**
   * `ArriboembarcacionesController.elimina_formato_revision_arribo_embarcacion()`
   */
  elimina_formato_revision_arribo_embarcacion: function (req, res) {

            Lista_avisos_arribo_emb.destroy({
                  id_formato: req.param('id_formato')
            })
            .exec(function (err, eliminados){
                  if (err) return res.negotiate(err); 
                  
                  return res.ok({success : true});
            });

  },

  /**
   * `ArriboembarcacionesController.genera_documento_pdf_arribo_emb()`
   */
  genera_documento_pdf_arribo_emb: function (req, res) {

                  Lista_avisos_arribo_emb.findOne({id_formato: req.param('id_formato')})
                  .populate('localidades')
                  .populate('puertos')
                  .populate('permisionario')
                  .populate('lugares_captura')
                  .sort('fecha DESC')
                  .sort('folio DESC')
                  .exec(function (err, lista) {

                        if (err) return res.negotiate(err); 
                        
                        if(!lista)
                        {
                            return res.ok({
                                total   : 0,
                                results : ''
                            });                      
                        }
                        else
                        {
                                  var dt = require('datetimejs');
                                  dt.AM = 'AM';
                                  dt.PM = 'PM';
                                  dt.MONTHS = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
                                  //dt.MNTH = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Agos','Sep','Oct','Nov','Dic'];

                                  var record = {
                                      id_formato                    : lista.id_formato,
                                      folio                         : lista.folio,
                                      fecha                         : dt.strftime(new Date(lista.fecha), '%d/%B/%Y'), 
                                      nombre_localidad              : lista.localidades.nombre_localidad,
                                      fecha_llegada                 : dt.strftime(new Date(lista.fecha_llegada), '%d/%B/%Y'),
                                      hora_llegada                  : dt.reformat(lista.hora_llegada, '%H:%M:%S', '%i:%M %p'),
                                      hora_arribo                   : dt.reformat(lista.hora_arribo, '%H:%M:%S', '%i:%M %p'),
                                      periodo_inicio                : dt.strftime(new Date(lista.periodo_inicio), '%d/%B/%Y'),
                                      periodo_fin                   : dt.strftime(new Date(lista.periodo_fin), '%d/%B/%Y'),
                                      nombre_puerto                 : lista.puertos.nombre_puerto,
                                      nombre_permisionario          : lista.permisionario.nombre_permisionario,
                                      clave_rnp                     : lista.permisionario.clave_rnp,
                                      numero_embarcaciones          : lista.numero_embarcaciones,
                                      lugar_captura                 : lista.lugares_captura.descripcion,
                                      zona_captura_litoral          : lista.zona_captura_litoral,
                                      zona_captura_bahia            : lista.zona_captura_bahia,
                                      zona_captura_aguas_estuarinas : lista.zona_captura_aguas_estuarinas,
                                      zona_captura_aguas_cont       : lista.zona_captura_aguas_cont,
                                      num_dias_efectivos            : lista.num_dias_efectivos,
                                      observaciones_revisor         : lista.observaciones_revisor,
                                      estatus                       : lista.estatus,
                                      fecha_envio_revision          : dt.strftime(new Date(lista.fecha_envio_revision), '%d/%B/%Y %i:%M %p'),
                                      fecha_aceptacion              : dt.strftime(new Date(lista.fecha_aceptacion), '%d/%B/%Y %i:%M %p')
                                  };


                                  Detalle_lista_avisos_arribo_emb.find({
                                    id_formato: req.param('id_formato')
                                  })
                                  .populate('especies')
                                  .exec(function (err, detalle) {
                                          var resultado = new Array();

                                          //Otra forma de recorrer el objeto y su contenido
                                          while (detalle.length)
                                          {
                                              var elemento = detalle.pop();

                                                  var record_detalle = {
                                                      id               : elemento.id,
                                                      id_especie       : elemento.especies.id_especie,
                                                      nombre_especie   : elemento.especies.nombre_especie,
                                                      clave_especie    : elemento.especies.clave_especie,
                                                      presentacion     : elemento.presentacion,
                                                      preservacion     : elemento.preservacion,
                                                      num_concesion    : elemento.num_concesion,                                          
                                                      fecha_expedicion : dt.strftime(new Date(elemento.fecha_expedicion), '%d/%B/%Y'), // dateFormat(elemento.fecha_expedicion, "UTC:dd/mmm/yyyy"),
                                                      vigencia_al      : dt.strftime(new Date(elemento.vigencia_al), '%d/%B/%Y'), //dateFormat(elemento.vigencia_al, "UTC:dd/mmm/yyyy"),
                                                      peso             : elemento.peso,
                                                      precio_kg        : elemento.precio_kg
                                                  };

                                                  resultado.push(record_detalle);
                                          };
                                          
                                        //Para ordenar el objeto por un dato en particular
                                        var resultado_ordenado = resultado.slice(0);
                                        resultado_ordenado.sort(function(a,b) {
                                            var x = a.nombre_especie.toLowerCase();
                                            var y = b.nombre_especie.toLowerCase();
                                            return x < y ? -1 : x > y ? 1 : 0;
                                        });

                                        record.detalle = resultado_ordenado;
                                        //sails.log.info(record);
                                        var fileFolder = 'archivos/pdf_generados/arribo_emb/';
                                        var fileName = 'f'+req.param('id_formato') + '.pdf';
                                        var filePath =  fileFolder + fileName;

                                        var fs = require('fs-extra');

                                        // Check if file exists
                                        fs.exists(filePath, function(exists) {
                                          if (exists)
                                          {  
                                                fs.unlink(filePath, function(){
                                                });
                                          }
                                              
                                                servicioPDF.generaPDF(record, 'arribo_emb', function(error){
                                                  if(error){
                                                    sails.log.error(error);
                                                    return res.ok({
                                                       estatus: -1
                                                    });
                                                  }
                                                  return res.ok({
                                                     estatus: 0,
                                                     fileid: fileName
                                                  });
                                                  
                                                });
                                        });

                                  });

                        }
                  });

  },

  /**
   * `ArriboembarcacionesController.descargapdf()`
   */
  descargapdfarriboemb: function (req, res) {
      var fileFolder = 'archivos/pdf_generados/arribo_emb/';
      var filePath =  fileFolder + req.param('fileid');
      res.download(filePath,req.param('fileid'));
  }

};

