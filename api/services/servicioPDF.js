        
        //var fs = require('fs');
        var ejs = require('ejs');
        var fs = require('fs-extra');

module.exports = {

    generaPDF: function(record, tipo, callback) {
        
          if(tipo == 'arribo_emb')
              var carpetaPDFgenerados = 'archivos/pdf_generados/arribo_emb/';
          else if(tipo == 'aviso_cosecha')  
              var carpetaPDFgenerados = 'archivos/pdf_generados/aviso_cosecha/';

          var nombreArchivoPDF = 'f'+record.id_formato + '.pdf';
          var rutaArchivoPDF =  sails.config.appPath + '/' + carpetaPDFgenerados + nombreArchivoPDF;

          servicioPDF.obtienePlatilla(record, tipo, function(error, template){
            if(error) return callback(error, null);
        
            //console.log(template);
            // affter check if the folder exists
            fs.mkdirp(carpetaPDFgenerados, function(err){
              if (err) return res.negotiate(err);

              sails.log.info('generando pdf');

              var wkhtmltopdf = require('wkhtmltopdf');
              
                      wkhtmltopdf(template, {
                        output: rutaArchivoPDF,
                        pageSize: 'letter',
                        encoding: 'iso-8859-1',
                        orientation: 'Landscape',
                        'margin-bottom': 0,
                        'margin-left': 0,
                        'margin-right': 0,
                        'margin-top': 0
                      }, function (code, signal) {
                        
                        if(code) sails.log.info(code);
                        if(signal) sails.log.info(signal);
                        
                        callback();
                      });
            });

          });



    },

    obtienePlatilla: function(data, nombrePlantilla, callback) {

          var plantillasDisponibles = servicioPDF.obtienePlatillaConfig();

          if(plantillasDisponibles[nombrePlantilla]){
            var nombreArchivoPDF = plantillasDisponibles[nombrePlantilla].nombreArchivoPDF;

            var rutaArchivoPDF = sails.config.appPath + '/' + 'archivos/plantillas_pdf/' + nombreArchivoPDF;

            fs.readFile(rutaArchivoPDF, 'utf-8', function(error, content) {
              if(error) return callback(error, null);

              var code = ejs.render(content, {
                data: data,
                path_logo_sagarpa: sails.getBaseurl() + '/images/sagarpa.jpg',
                path_logo_conapesca: sails.getBaseurl() + '/images/conapesca.jpg'
                //bgFileUrl: bgFileUrl
              });

              callback(null, code);
            });

          }else{
            callback('HTML Template for PDF not found: '+ nombrePlantilla, null);
          }

    },

    obtienePlatillaConfig: function() {
          var plantillasDisponibles = {};

          plantillasDisponibles.arribo_emb = {};
          plantillasDisponibles.arribo_emb.label = 'Arribo de embarcaciones';
          plantillasDisponibles.arribo_emb.nombreArchivoPDF = 'plantilla_formato_arribo_emb.ejs';

          plantillasDisponibles.aviso_cosecha = {};
          plantillasDisponibles.aviso_cosecha.label = 'Aviso de cosecha';
          plantillasDisponibles.aviso_cosecha.nombreArchivoPDF = 'plantilla_formato_aviso_cosecha.ejs';

          return plantillasDisponibles;
    }


};