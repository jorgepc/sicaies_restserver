/**
* Peticiones_inicio_sesion.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  tableName: 'peticiones_inicio_sesion',
  attributes: {
		id_peticion : { type: 'integer', autoIncrement: true, unique: true, primaryKey: true},
		ip          : { type: 'string', size: 15 },
		key_inicio  : { type: 'string', size: 128},
		fecha       : { type: 'datetime', defaultsTo: function (){ return new Date();}  },
		logueado    : { type: 'boolean', defaultsTo: 0 },
		usuario     : { model: 'Usuarios', columnName: 'id_usuario'}
  },

  registra_peticion: function (inputs, cb) {
	    if(inputs.tki === ''||inputs.tki === undefined)
	    {
		    var uuid = require('node-uuid');
		    var randomid = uuid.v4();

		    var crypto = require('crypto');
			var key_inicio = crypto.createHash('sha512').update(randomid).digest("hex");

		    Peticiones_inicio_sesion.create({
		      ip: inputs.ip,
		      key_inicio: key_inicio
		    })
		    .exec(cb);
	    }
	    else
	    {
		    Peticiones_inicio_sesion.findOne({key_inicio:inputs.tki}).exec(function (err, peticion) {
		      if (err) 
		      		return cb(err);
		      
		      if (!peticion) {
		      		//return cb(new Error('User not found.'));
			      	err = new Error();
			      	err.msg = require('util').format('Acceso a la aplicacion no autorizado');
			      	err.status = 404;
			      	return cb(err);
		      }
		      else
		      {
		      		cb(null, peticion);
		      }

		    });
	    }
  }



};


