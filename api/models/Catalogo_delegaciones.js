/**
* Catalogo_delegaciones.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  tableName: 'cat_delegaciones',
  attributes: {
	id_delegacion     : { type: 'integer', autoIncrement: true, unique: true, primaryKey: true },
	nombre_delegacion : { type: 'string', size: 45, defaultsTo: '' }
  }

};

