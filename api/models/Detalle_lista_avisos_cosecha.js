/**
* Detalle_lista_avisos_cosecha.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	  tableName: 'f_aviso_cosecha_registro',
	  attributes: {
			id               : { type: 'integer', unique: true, primaryKey: true, autoIncrement: true},
			formato    		 : { model: 'Lista_avisos_cosecha', columnName: 'id_formato' },
			especies         : { model: 'Catalogo_especies_cosecha', columnName: 'id_especie' },
			presentacion     : { type: 'string', size: 35, defaultsTo: ''},
			volumen          : { type: 'float', defaultsTo: 0},
			precio_kg        : { type: 'float', defaultsTo: 0}
	  }

};

