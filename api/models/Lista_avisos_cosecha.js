/**
* Lista_avisos_cosecha.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

		tableName: 'f_aviso_cosecha',
		attributes: {
			id_formato              : { type: 'integer', unique: true, primaryKey: true, autoIncrement: true},
			folio                   : { type: 'string', size: 20},
			fecha                   : { type: 'date'},
			localidades             : { model: 'Catalogo_localidades', columnName: 'id_localidad' },
			delegaciones            : { model: 'Catalogo_delegaciones', columnName: 'id_delegacion' },
			oficinas                : { model: 'Catalogo_oficinas', columnName: 'id_oficina' },
			permisionario           : { model: 'Permisionarios', columnName: 'id_permisionario' },
			num_concesion           : { type: 'string', size: 10, defaultsTo: ''},
			ubicacion_instalaciones : { type: 'string', size: 65, defaultsTo: ''},
			municipios              : { model: 'Catalogo_municipios', columnName: 'id_municipio' },
			fecha_concesion         : { type: 'date'},
			domicilio               : { type: 'string', size: 145, defaultsTo: ''},
			estados                 : { model: 'Catalogo_estados', columnName: 'id_estado' },
			telefono                : { type: 'string', size: 10, defaultsTo: ''},
			observaciones_revisor   : { type: 'text', defaultsTo: ''},
			estatus                 : { type: 'integer', defaultsTo: 1},
			fecha_envio_revision    : { type: 'datetime'},
			fecha_aceptacion        : { type: 'datetime'},
			detalle: {
				collection: 'Detalle_lista_avisos_cosecha',
				via: 'formato'
			}

		},

		afterCreate: function (newlyInsertedRecord, next) {

			var folio = newlyInsertedRecord.id_formato.toString();

		    Lista_avisos_cosecha.update({
		        id_formato : newlyInsertedRecord.id_formato
		    },
		    {
		        folio : folio
		    })
		    .exec(function (err, lista){
		    	if(err) return next(err);		    	
		    	next();
		    });
		    
		    //cb();
		},
	
	    afterDestroy: function(destroyedRecords, cb) {
	        // Destroy any child whose teacher has an ID of one of the 
	        // deleted teacher models
	        Detalle_lista_avisos_cosecha.destroy({formato: _.pluck(destroyedRecords, 'id_formato')}).exec(cb);
	    }

};