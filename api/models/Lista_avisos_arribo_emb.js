/**
* Lista_avisos_arribo_emb.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

		tableName: 'f_aviso_arribo_emb',
		attributes: {
			id_formato                    : { type: 'integer', unique: true, primaryKey: true, autoIncrement: true},
			folio                         : { type: 'string', size: 20},
			fecha                         : { type: 'date'},
			localidades                   : { model: 'Catalogo_localidades', columnName: 'id_localidad' },
			fecha_llegada                 : { type: 'date'},
			hora_llegada                  : { type: 'string', size: 12},
			hora_arribo                   : { type: 'string', size: 12},
			periodo_inicio                : { type: 'date'},
			periodo_fin                   : { type: 'date'},
			puertos                       : { model: 'Catalogo_puertos', columnName: 'id_puerto' },
			permisionario                 : { model: 'Permisionarios', columnName: 'id_permisionario' },
			numero_embarcaciones          : { type: 'integer'},
			lugares_captura               : { model: 'Catalogo_lugares_captura', columnName: 'id_lugar_captura' },
			zona_captura_litoral          : { type: 'integer', defaultsTo: 0},
			zona_captura_bahia            : { type: 'integer', defaultsTo: 0},
			zona_captura_aguas_estuarinas : { type: 'integer', defaultsTo: 0},
			zona_captura_aguas_cont       : { type: 'integer', defaultsTo: 0},
			num_dias_efectivos            : { type: 'integer', defaultsTo: 0},
			observaciones_revisor         : { type: 'text', defaultsTo: ''},
			estatus                       : { type: 'integer', defaultsTo: 1},
			fecha_envio_revision          : { type: 'datetime'},
			fecha_aceptacion              : { type: 'datetime'},
			detalle: {
				collection: 'Detalle_lista_avisos_arribo_emb',
				via: 'formato'
			}

		},

		afterCreate: function (newlyInsertedRecord, next) {

			var folio = newlyInsertedRecord.id_formato.toString();

		    Lista_avisos_arribo_emb.update({
		        id_formato : newlyInsertedRecord.id_formato
		    },
		    {
		        folio : folio
		    })
		    .exec(function (err, lista){
		    	if(err) return next(err);		    	
		    	next();
		    });
		    
		    //cb();
		},
	
	    afterDestroy: function(destroyedRecords, cb) {
	        // Destroy any child whose teacher has an ID of one of the 
	        // deleted teacher models
	        Detalle_lista_avisos_arribo_emb.destroy({formato: _.pluck(destroyedRecords, 'id_formato')}).exec(cb);
	    }


};
