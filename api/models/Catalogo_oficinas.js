/**
* Catalogo_oficinas.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  tableName: 'cat_oficinas',
  attributes: {
	id_oficina     : { type: 'integer', autoIncrement: true, unique: true, primaryKey: true },
	id_delegacion  : { type: 'integer', defaultsTo: 0},
	nombre_oficina : { type: 'string', size: 45, defaultsTo: '' }
  }

};

