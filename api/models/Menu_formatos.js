/**
* Menu_formatos.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	  tableName: 'menu_formatos',
	  attributes: {
			nodo_id   : { type: 'integer', columnName: 'nodo_id', autoIncrement: true, unique: true, primaryKey: true},
			opcion_id : { type: 'varchar', size: 45},
			text      : { type: 'string', size: 45},
			parent_id : { type: 'integer'},
			leaf      : { type: 'boolean'},
			orden     : { type: 'integer'},
			tipo      : { type: 'string', size: 1},
			permisionarios: {
				collection: 'Permisionarios',
				via: 'formatos'
			}

	  }
};

//Menu_formatos.find({tipo: 'M'}).populate('permisionarios',{id_permisionario: 1}).exec(console.log);