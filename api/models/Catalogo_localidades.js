/**
* Catalogo_localidades.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  tableName: 'cat_localidades',
  attributes: {
	id_localidad     : { type: 'integer', autoIncrement: true, unique: true, primaryKey: true },
	nombre_localidad : { type: 'string', size: 55, defaultsTo: '' }
  }
};

