/**
* Catalogo_puertos.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  tableName: 'cat_puertos',
  attributes: {
	id_puerto     : { type: 'integer', autoIncrement: true, unique: true, primaryKey: true },
	nombre_puerto : { type: 'string', size: 65, defaultsTo: '' }
  }
};

