/**
* Usuarios.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	  tableName: 'usuarios',
	  attributes: {
			id_usuario       : { type: 'integer', autoIncrement: true, unique: true, primaryKey: true},
			permisionario    : { model: 'Permisionarios', columnName: 'id_permisionario', defaultsTo: 0 },
			nombre           : { type: 'string', size: 65, defaultsTo: ''},
			usuario          : { type: 'string', size: 30, defaultsTo: ''},
			pass             : { type: 'string', size: 128, defaultsTo: ''},
			perfil           : { type: 'integer', defaultsTo: 1},
			logueado         : { type: 'boolean', defaultsTo: 0},
			fecha_logueo     : { type: 'datetime'},
			token_sesion     : { type: 'string', size: 128},
			fecha_salida     : { type: 'datetime'}
	  }

};
