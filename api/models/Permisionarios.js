/**
* Permisionarios.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	  tableName: 'cat_permisionarios',
	  attributes: {
			id_permisionario     : { type: 'integer', autoIncrement: true, unique: true, primaryKey: true},
			nombre_permisionario : { type: 'string', size: 165, defaultsTo: ''},
			clave_rnp            : { type: 'string', size: 20, defaultsTo: ''},
			tipo_permisionario   : { type: 'string', size: 1, enum: ['A', 'P'] },
			usuarios: {
				collection: 'Usuarios',
				via: 'permisionario'
			},
			formatos: {
				collection: 'Menu_formatos',
				via: 'permisionarios',
				dominant: true
			}
	  }
};


// Permisionarios.findOne({id_permisionario:1}).populate('formatos').exec(function(err, permisionario) { permisionario.formatos.add(3); permisionario.save(function(err) {}); });