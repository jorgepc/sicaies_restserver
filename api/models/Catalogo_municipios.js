/**
* Catalogo_municipios.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  tableName: 'cat_municipios',
  attributes: {
	id_municipio     : { type: 'integer', autoIncrement: true, unique: true, primaryKey: true },
	nombre_municipio : { type: 'string', size: 35, defaultsTo: '' }
  }

};

