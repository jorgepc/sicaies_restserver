/**
* Catalogo_lugares_captura.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  tableName: 'cat_lugares_captura',
  attributes: {
	id_lugar_captura : { type: 'integer', autoIncrement: true, unique: true, primaryKey: true },
	descripcion      : { type: 'string', size: 254, defaultsTo: '' },
	lat              : { type: 'float', defaultsTo: 0 },
	long             : { type: 'float', defaultsTo: 0 }
  }
};

