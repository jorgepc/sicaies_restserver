/**
* Catalogo_estados.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  tableName: 'cat_estados',
  attributes: {
	id_estado     : { type: 'integer', autoIncrement: true, unique: true, primaryKey: true },
	nombre_estado : { type: 'string', size: 35, defaultsTo: '' }
  }

};

