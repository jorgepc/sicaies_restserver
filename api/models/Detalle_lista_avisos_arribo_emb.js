/**
* Detalle_lista_avisos_arribo_emb.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	  tableName: 'f_aviso_arribo_emb_registro',
	  attributes: {
			id               : { type: 'integer', unique: true, primaryKey: true, autoIncrement: true},
			formato    		 : { model: 'Lista_avisos_arribo_emb', columnName: 'id_formato' },
			especies         : { model: 'Catalogo_especies_pesca', columnName: 'id_especie' },
			presentacion     : { type: 'string', size: 35, defaultsTo: ''},
			preservacion     : { type: 'string', size: 45, defaultsTo: ''},
			num_concesion    : { type: 'string', size: 12, defaultsTo: ''},
			fecha_expedicion : { type: 'date'},
			vigencia_al      : { type: 'date'},
			peso             : { type: 'float', defaultsTo: 0},
			precio_kg        : { type: 'float', defaultsTo: 0}
	  }

};
