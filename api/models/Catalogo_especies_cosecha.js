/**
* Catalogo_especies_pesca.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  tableName: 'cat_especies_cosecha',
  attributes: {
	id_especie        : { type: 'integer', autoIncrement: true, unique: true, primaryKey: true},
	nombre_especie    : { type: 'string', size: 145, defaultsTo: ''},
	clave_especie     : { type: 'string', size: 12, defaultsTo: '' },
	nombre_cientifico : { type: 'string', size: 45, defaultsTo: '' }
  }
  
};

